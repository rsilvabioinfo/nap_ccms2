#!/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/python

import os
import shutil
import json
import sys

def main():
    #shutil.copytree("/data/beta-proteomics2/tools/nap_ccms2/Snap", 'Snap')
    #shutil.copytree("/data/beta-proteomics2/tools/nap_ccms2/DB", 'DB')

    os.environ['PATH'] += ':/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/'
    os.environ['JAVA_HOME'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre'
    os.environ['LD_LIBRARY_PATH'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre/lib/amd64/server'
    os.system('/data/beta-proteomics2/tools/nap_ccms2/Snap/metfrag.out.R')

if __name__ == "__main__":
    main()

