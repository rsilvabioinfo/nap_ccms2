require("data.table")
load.db <- function(path, type) {
       switch(type,
              HMDB = fread(paste0(path, "DB/hmdb_21_02_2018_formatted.txt")),
              GNPS = fread(paste0(path, "DB/gnps_29_10_2019.txt")),
              DNP = fread(paste0(path, "DB/dnpCLASS.txt")),
              DNP_PUBLIC = fread(paste0(path, "DB/dnp_public.tsv")),
              CHEBI = fread(paste0(path, "DB/chebiCLASS.txt")),
              SUPNAT = fread(paste0(path, "DB/supernaturalCLASS.txt")),
              # still has NAs
              DRUGBANK = fread(paste0(path, "DB/drugdb_formatted.txt")),
              MarinLit = fread(paste0(path, "DB/MarinLit_04_03_2018.txt")),
              FooDB = fread(paste0(path, "DB/fooddb_04_03_2018.txt")),
              NPAtlas = fread(paste0(path, "DB/np_atlas_2019_02.tsv")),
              COCONUT = fread(paste0(path, "DB/COCONUT.txt")),
              mock = fread(paste0(path, "DB/mockCLASS.txt"))
	)
     }

