#!/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/python

import os
import shutil
import sys
import re
import multiprocessing
import math 

#def main():
    #if not bool(re.search("line", sys.argv[1])):
    #    exit()

with open(sys.argv[1]) as f:
    lines = f.read().splitlines() 

#shutil.copytree("/data/beta-proteomics2/tools/nap_ccms2/Snap", 'Snap')

# FIX
#fls = os.listdir('split_data')
#if sum([x == 'sl_mock_parameter.txt' for x in fls]):
#    shutil.copy("split_data/sl_mock_parameter.txt", 'Snap/sl_mock_parameter.txt')
#    print("Found parameter file") 

os.environ['PATH'] += ':/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/'
os.environ['JAVA_HOME'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre'
os.environ['LD_LIBRARY_PATH'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre/lib/amd64/server'

def do_line(line):
	os.system("/data/beta-proteomics2/tools/nap_ccms2/Snap/get.metfrag.single.R -f " + 'split_data/' + line + " -O " + "fragmenter_res/" +line)

if __name__ == "__main__":
#    main()
    #pool = multiprocessing.Pool(math.ceil(multiprocessing.cpu_count()/2))
    #pool = multiprocessing.Pool()
    #results = pool.map(do_line, lines)
    for line in lines:
       do_line(line)

 
