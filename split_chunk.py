#!/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/python

import os
import sys
import re
import math 

def main():
    fls = os.listdir('split_data')
    fls = [x for x in fls  if bool(re.match('line', x))]
    fls = sorted(fls, key=lambda e: int(re.sub('\\D', '', e)))

    chunksize = 15
    if len(fls)/15 > 100: 
        chunksize = math.ceil(len(fls)/100) 
    
    k = 1
    for i in range(0, len(fls), chunksize):
        to_save = fls[i:i+chunksize]
        with open('chunk_data/chunk' + str(k) + '.txt', 'w') as fp:
            for item in to_save:
                fp.write("%s\n" % item)
     
        k += 1
    

if __name__ == "__main__":
    main()

