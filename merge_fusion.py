#!/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/python

import os
import shutil
import sys
import re
import math 

def main():
    #shutil.copytree("/data/beta-proteomics2/tools/nap_ccms2/Snap", 'Snap')

    os.environ['PATH'] += ':/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/'
    os.environ['JAVA_HOME'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre'
    os.environ['LD_LIBRARY_PATH'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre/lib/amd64/server'
    os.system("/data/beta-proteomics2/tools/nap_ccms2/Snap/load.fusion.R -d " + sys.argv[1])

    fls = os.listdir('fusion_res')
    fls = [x for x in fls  if bool(re.match('line', x))]
    fls = sorted(fls, key=lambda e: int(re.sub('\\D', '', e)))

    chunksize = 15
    if len(fls)/15 > 30: 
        chunksize = math.ceil(len(fls)/30) 
    
    k = 1
    for i in range(0, len(fls), chunksize):
        to_save = fls[i:i+chunksize]
        with open('chunk_fus/chunk' + str(k) + '.txt', 'w') as fp:
            for item in to_save:
                fp.write("%s\n" % item)
     
        k += 1

if __name__ == "__main__":
    main()

