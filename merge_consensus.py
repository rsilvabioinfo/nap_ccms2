#!/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/python

import os
import shutil
import sys
import re

def main():
    #shutil.copytree("/data/beta-proteomics2/tools/nap_ccms2/Snap", 'Snap')

    os.environ['PATH'] += ':/data/beta-proteomics2/tools/miniconda3/envs/nap/bin/'
    os.environ['JAVA_HOME'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre'
    os.environ['LD_LIBRARY_PATH'] = '/data/beta-proteomics2/tools/miniconda3/envs/nap/jre/lib/amd64/server'
    os.system("/data/beta-proteomics2/tools/nap_ccms2/Snap/load.fusion.R -d " + sys.argv[1])

if __name__ == "__main__":
    main()

